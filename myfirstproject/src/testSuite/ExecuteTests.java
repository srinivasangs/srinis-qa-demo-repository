package testSuite;
import java.util.concurrent.TimeUnit;

import objectRepository.AtlassianHome;
import objectRepository.DashboardPage;
import objectRepository.PageInput;
import objectRepository.SetRestrictionsWindow;
import objectRepository.ViewPage;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.ExcelUtils;

public class ExecuteTests {
	WebDriver driver;
	
  @Test(dataProvider = "TestDataArray",priority=0)
  public void VerifyPageCreation(String BrowserName, String UserName, String Password, String Url) throws InterruptedException{
		LoginTest(BrowserName,UserName,Password,Url);//Login to the application
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	String AutoGenPageTitle = "Page from Automated Test - "+utilities.RandomNumbers.GetRandomNumbers(5); //Store the value of the page title in a variable
	 // Thread.sleep(8000);
	  
	  DashboardPage.linkCreate(driver).click(); //Click on the "Create" link
	  //Thread.sleep(5000);

	PageInput.txtPageTitle(driver).sendKeys(AutoGenPageTitle); //Enter the page title
	PageInput.txtPageTitle(driver).sendKeys(Keys.RETURN);//Hit Enter
	PageInput.btnSave(driver).click();//Click Save button
	
    //Thread.sleep(5000); //Wait for the page objects to load
	String PageTitle;
    PageTitle = ViewPage.linkPageTitle(driver).getText();//Store the runtime data (page title) from the application
    
    Assert.assertTrue("Pass - Page successfully created", PageTitle.equalsIgnoreCase(AutoGenPageTitle)); // Compare the page title
  }
  
  
  @Test(dataProvider = "TestDataArray")
  public void VerifySetRestrictions(String BrowserName, String UserName, String Password, String Url) throws InterruptedException{
	 
		  LoginTest(BrowserName,UserName,Password,Url);
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  //Thread.sleep(5000);
        //DashboardPage.linkSpaceMenu(driver).click();
        //DashboardPage.linkRecentSpaces(driver).click();
		  //Thread.sleep(5000);

	    DashboardPage.linkPage(driver).click();      //Click on the first page link in the dashboard stream
		  //Thread.sleep(8000);

	    if (ViewPage.iconUnlock(driver).isDisplayed())
	    {
	    	String objectTitle =  ViewPage.iconUnlock(driver).getAttribute("class");
	    	//System.out.println("Inside Lock");
			  //Thread.sleep(8000);
	    	System.out.println("object title is "+objectTitle);
	    	ViewPage.iconUnlock(driver).click();
	    	//Thread.sleep(5000);
	    	if (objectTitle.equalsIgnoreCase("aui-icon aui-icon-small aui-iconfont-locked restricted"))
	    	{
	    		System.out.println("from Restricted scenario - "+objectTitle);
	    	SetRestrictionsWindow.elementRestrictionsList(driver).click(); //Click on the Restrictions options list
			SetRestrictionsWindow.elementNoRestrictionsValue(driver).click(); //Select the no restriction option - This could be parameterized if required.
	    	}
	    	else if(objectTitle.equalsIgnoreCase("aui-icon aui-icon-small aui-iconfont-unlocked"))
	    	{
	    		System.out.println("from Unrestricted scenario");
	    	    SetRestrictionsWindow.elementRestrictionsList(driver).click(); //Click on the Restrictions options list
			    SetRestrictionsWindow.elementRestrictionsValue(driver).click(); //Select the restriction option - This could be parameterized if required.
	       	}
	    	//Thread.sleep(5000);
    		SetRestrictionsWindow.btnApply(driver).click(); //Click on Apply button
			Assert.assertTrue("Pass - The restrictions are updated",(ViewPage.iconUnlock(driver).isDisplayed())); //Verify if the restrictions are applied
	    }
	    else
	    {
	    	Assert.assertFalse("Fail - The lock icon is not displayed to update restrictions.", ViewPage.iconUnlock(driver).isDisplayed());
	    }
		//Thread.sleep(5000);
		//ViewPage.linkUserMenu(driver).click();

		//ViewPage.linkLogOut(driver).click();
		//ViewPage.btnLogOutConfirm(driver).click();
	  
  }
 public void LoginTest(String BrowserName, String UserName, String Password, String Url) throws InterruptedException
  {
	  //Thread.sleep(5000);

	  //BrowserActions.initDriver(driver, BrowserName);
 	if (BrowserName.equalsIgnoreCase("Firefox"))
		{
			driver = new FirefoxDriver();
		}else if (BrowserName.equalsIgnoreCase("Chrome"))
		{
			//Configuring Chrome Driver
			System.setProperty("webdriver.chrome.driver",System.getProperty(("user.dir")) + "\\src\\Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if (BrowserName.equalsIgnoreCase("IE"))
		{
			//Configuring IE Driver
			System.setProperty("webdriver.ie.driver", System.getProperty(("user.dir")) + "\\src\\Drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}else
		{
			System.out.println("Incorrect browser. Please use either of Firefox, Chrome or IE. You used "+BrowserName);
		}
 	driver.manage().window().maximize();
	  //System.out.println("Driver title - "+driver.getTitle());
	  driver.get(Url);
	  //driver.manage().window().maximize();
	  //Thread.sleep(5000);

	    AtlassianHome.txtUserName(driver).sendKeys(UserName);
		AtlassianHome.txtPassword(driver).sendKeys(Password);
		AtlassianHome.btnLogin(driver).click();
		
		//DashboardPage.linkCreate(driver).click(); 
  }

  @DataProvider
  public Object[][] TestDataArray()  throws Exception{
	  String filePath = (System.getProperty(("user.dir")) + "\\src\\TestData.xlsx");
	  //System.out.println("inside DataProvider"+System.getProperty("user.dir")+"\\src\\TestData.xlsx");
      Object[][] testObjArray = ExcelUtils.TestDataArray(filePath, "Login");
      return testObjArray;
  }

  @AfterMethod
  public void Logout() throws InterruptedException {
	  //Thread.sleep(5000);
	  ViewPage.linkUserMenu(driver).click();
      ViewPage.linkLogOut(driver).click();
	  ViewPage.btnLogOutConfirm(driver).click();
	  driver.close();
  }

}
