package objectRepository;
 
import org.openqa.selenium.By;
 
import org.openqa.selenium.WebDriver;
 
import org.openqa.selenium.WebElement;
public class DashboardPage {
WebDriver driver;
      
       //Define the WebDriver
       public DashboardPage(WebDriver driver)
       {
              this.driver = driver;
       }
       private static WebElement element = null;
      
       //Define the Create element in the dashboard page which could be used from any other class that imports DashboardPage class.
       public static WebElement linkCreate(WebDriver driver)
       {
              element = driver.findElement(By.id("quick-create-page-button"));
              return element;
       }
       //Define the Page element in the dashboard page which could be used from any other class that imports DashboardPage class.
       public static WebElement linkPage(WebDriver driver)
       {
              element = driver.findElement(By.partialLinkText("Page from Automated Test"));
              return element;
       }
       //Define the Space Menu Link element in the dashboard page which could be used from any other class that imports DashboardPage class.
       public static WebElement linkSpaceMenu(WebDriver driver)
       {
              element = driver.findElement(By.id("space-menu-link"));
              return element;
       }
       //Define the Recent Spaces element in the dashboard page which could be used from any other class that imports DashboardPage class.
       public static WebElement linkRecentSpaces(WebDriver driver)
       {
              element = driver.findElement(By.xpath("//*[@id='recent-spaces-section']/ul/li[1]/a"));
              return element;
       }
}