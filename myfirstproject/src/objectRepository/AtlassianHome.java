package objectRepository;
import org.openqa.selenium.By;
 
import org.openqa.selenium.WebDriver;
 
import org.openqa.selenium.WebElement;
import utilities.BrowserActions;
 
public class AtlassianHome {
      
       WebDriver driver;
      
       private static WebElement element = null;
       //Define the Atlassian logo in the home page which could be used from any other class that imports Atlassian Home class.
       public static WebElement imgAtlassianLogo(WebDriver driver)
       {
              element = driver.findElement(By.className("aui-header-logo-device indra-ondemand-logo"));
              return element;
       }
       //Define the Username field in the home page which could be used from any other class that imports Atlassian Home class.
    public static WebElement txtUserName(WebDriver driver)
    {
           element = driver.findElement(By.id("username"));
              return element;
    }
    //Define the Username field in the home page which could be used from any other class that imports Atlassian Home class.
    public static WebElement txtPassword(WebDriver driver)
    {
           element = driver.findElement(By.id("password"));
              return element;
    }
 
    //Define the Login Button in the home page which could be used from any other class that imports Atlassian Home class.
    public static WebElement btnLogin(WebDriver driver)
    {
           element = driver.findElement(By.id("login"));
              return element;
    }
  }
 