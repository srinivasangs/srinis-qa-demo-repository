package objectRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class SetRestrictionsWindow {
       //Define the WebDriver
              WebDriver driver;
 
                     private static WebElement element = null;
                     //Define the list element in the 'Restrictions' page which could be used from any other class that imports SetRestrictionsWindow class.
                     public static WebElement elementRestrictionsList(WebDriver driver)
                     {
                            element = driver.findElement(By.xpath("//*[@id='s2id_page-restrictions-dialog-selector']/a/span[1]/div/span[2]"));
                           return element;
                     }
                 
                     //Define the list value element in the 'Restrictions' page which could be used from any other class that imports SetRestrictionsWindow class.
                     public static WebElement elementRestrictionsValue(WebDriver driver)
                     {
                            element = driver.findElement(By.xpath("//*[@id='select2-drop']/ul/li[2]/div/div"));
                           return element;
                     }      
                   //Define the list value element in the 'Restrictions' page which could be used from any other class that imports SetRestrictionsWindow class.
                     public static WebElement elementNoRestrictionsValue(WebDriver driver)
                     {
                            element = driver.findElement(By.xpath("//*[@id='select2-drop']/ul/li[1]/div/div"));
                           return element;
                     }      
                     
                     //Define the User's name link in the 'Restrictions' page which could be used from any other class that imports SetRestrictionsWindow class.
                     public static WebElement linkUserName(WebDriver driver)
                     {
                           element = driver.findElement(By.className("confluence-userlink userlink-0"));
                           return element;
                     }
                    
                     //Define the Apply button in the 'Restrictions' page which could be used from any other class that imports SetRestrictionsWindow class.
                     public static WebElement btnApply(WebDriver driver)
                     {
                           element = driver.findElement(By.id("page-restrictions-dialog-save-button"));
                           return element;
                     }
                    
 
}