package objectRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class ViewPage {
       //Define the WebDriver
                     WebDriver driver;
 
                           private static WebElement element = null;
                           //Define the Title element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement linkPageTitle(WebDriver driver)
                           {
                                  element = driver.findElement(By.id("title-text"));
                                  if (element.equals(null))
                                		  {
                                	         element = driver.findElement(By.partialLinkText("Page 1 from framework"));
                                		  }
                                  return element;
                           }
                           //Define the Content element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement linkPageDescription(WebDriver driver)
                           {
                                  element = driver.findElement(By.id("main-content"));
                                  return element;
                           }
                           //Define the Unlock icon element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement iconUnlock(WebDriver driver)
                           {
                               element = driver.findElement(By.id("content-metadata-page-restrictions"));
                        	   return element;
                        	   
                           }
                           //Define the Lock icon element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement iconLock(WebDriver driver)
                           {
                                  element = driver.findElement(By.xpath("//*[@class='aui-icon aui-icon-small aui-iconfont-locked restricted']"));
                                  return element;
                           }                         
                          
                           //Define the Options icon element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement linkUserMenu(WebDriver driver)
                           {
                                  element = driver.findElement(By.id("user-menu-link"));
                                  return element;
                           }     
                           //Define the Logout element in the View page which could be used from any other class that imports ViewPage class.
                           public static WebElement linkLogOut(WebDriver driver)
                           {
                                  element = driver.findElement(By.linkText("Log Out"));
                                  return element;
                           } 
                           //Define the Logout Confirmation element in the Logout which could be used from any other class that imports ViewPage class.
                           public static WebElement btnLogOutConfirm(WebDriver driver)
                           {
                                  element = driver.findElement(By.id("logout"));
                                  return element;
                           } 
                                                      
                           
}