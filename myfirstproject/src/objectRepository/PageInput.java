package objectRepository;
import org.openqa.selenium.By;
 
import org.openqa.selenium.WebDriver;
 
import org.openqa.selenium.WebElement;
public class PageInput {
       //Define the WebDriver
       WebDriver driver;
 
              private static WebElement element = null;
             
              //Define the Page Title element in the create page which could be used from any other class that imports PageInput class.
              public static WebElement txtPageTitle(WebDriver driver)
              {
                     element = driver.findElement(By.id("content-title"));
                     return element;
              }
 
              //Define the Page Description element in the create page which could be used from any other class that imports PageInput class.
              public static WebElement txtPageDescription(WebDriver driver)
              {
                     element = driver.findElement(By.id("tinymce"));
                     return element;
              }
 
              //Define the Save element in the create page which could be used from any other class that imports PageInput class.
              public static WebElement btnSave(WebDriver driver)
              {
                     element = driver.findElement(By.id("rte-button-publish"));
                     return element;
              }
             
             
}