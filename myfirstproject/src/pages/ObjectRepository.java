package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ObjectRepository {
  WebDriver driver;
 public ObjectRepository(WebDriver driver)
 {
  this.driver = driver;
 }
 
  //Get the objects from Atlassian home page defined
      By logoHomePage = By.className("aui-header-logo-device indra-ondemand-logo");
	  By txtUserName = By.id("username");
	  By txtPassword = By.id("password");
	  By btnLogin = By.id("login");
  
  //Get the objects from Dashboard page defined
 
	  By hdrDashBoardPageTitle = By.id("title-text");
	  By btnCreatePage = By.id("quick-create-page-button");
  
  //Get the objects from the page creation input page
      By txtPageTitle = By.id("content-title");
      By txtPageContent = By.xpath("//*[@id='tinymce']/p");
      By btnSave = By.id("rte-button-publish");
      
  //Get the objects from Space page which displays the created page
      By linkPageTitle = By.xpath("//*[@id='title-text']/a");
      By paraPageContent = By.xpath("//*[@id='main-content']/p");
      By iconUnlock = By.className("aui-icon aui-icon-small aui-iconfont-unlocked");//original-title="Unrestricted"
      By iconLock = By.className("aui-icon aui-icon-small aui-iconfont-locked restricted");
  
  // Get the objects from Restrictions window
      By txtRestrictionsHeader = By.className("aui-dialog2-header-main");
      By selectRestrictions = By.xpath("//*[@id='s2id_page-restrictions-dialog-selector']/a");
      By btnSavePage = By.id("page-restrictions-dialog-save-button");
      

  
  
  
  
}
