package utilities;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

    
                                private static XSSFSheet ExcelSheet;
 
                                private static XSSFWorkbook ExcelWorkBook;
 
                                private static XSSFCell Cell;
 
                                private static XSSFRow Row;
                                static String[][] DataArray = null;
                               	static int colCount;
                                //This method is to initialize the Excel file and related objects.
 
                                public static void setExcelFile(String Path,String SheetName) throws Exception {
 
                                                try {
 
                                                // Open the Excel file
                                                FileInputStream ExcelFile = new FileInputStream(Path);
                                                // Access the required test data sheet
                                                ExcelWorkBook = new XSSFWorkbook(ExcelFile);
                                                ExcelSheet = ExcelWorkBook.getSheet(SheetName);
                                                } catch (Exception e){
                                                  throw (e);
                                                }
                                   }
 
                                //Read data from the Excel cell
 
                    public static String getCellData(int RowNum, int ColNum) throws Exception{
 
                                                try{
 
                                                Cell = ExcelSheet.getRow(RowNum).getCell(ColNum);
 
                                                String CellData = Cell.getStringCellValue();
 
                                                return CellData;
 
                                                }catch (Exception e)
                                                {
                                                 throw (e);
                                                 //return "";
 
                                                }
 
                                    }
 
                    
                    
                    
                    
                    public static int getColumnCount(String FilePath, String SheetName) throws Exception{
                    try{
                        colCount = 0;
                    	setExcelFile(FilePath, SheetName);
                    	for(int i=0;i<4;i++)
                    	{
                    		
                    		String ColumnName = ExcelUtils.getCellData(0,i);
                    		if (ColumnName==null)
                    		{
                    			colCount = i-1;
                    			break;
                    		}
                    	} 
                    }catch (Exception e)
                        {
                        	System.out.println("Exception found - "+e);
                        }
                        return 4;
                  	
                   }
                    public static String[][] TestDataArray(String FilePath, String SheetName) throws Exception{
                   try {
                	   //System.out.println("Inside TestData Array"+ FilePath);
                    	setExcelFile(FilePath, SheetName);
                    	int rowCount = ExcelSheet.getLastRowNum();                
                    	int columnCount = getColumnCount(FilePath, SheetName);
                    	DataArray=new String[rowCount][columnCount];
                    	int ci=0;
                    	for (int i=1;i<=rowCount;i++,ci++)
                    	{              	
                    		int cj=0;
                    	    for (int j=0;j<columnCount;j++,cj++)
                    	    {
                    	    	DataArray[ci][cj] = getCellData(i, j);
                    	    	
                    	    }
                    	}		
                    	
                    	
                    } catch (Exception e)
                    {
                    	System.out.println("Exception found - "+e);
                    }
                   
                   return (DataArray);
                   }    
                   
                    
                                //This method is to write in the Excel cell, Row num and Col num are the parameters
 
                                public static void setCellData(String Output,  int RowNum, int ColNum) throws Exception      
                                {
                                               try{
                                                Row  = ExcelSheet.getRow(RowNum);
                                                Cell = Row.getCell(ColNum);
                                                if (Cell == null)
                                                {
                                                   Cell = Row.createCell(ColNum);
                                                   Cell.setCellValue(Output);
                                                } 
                                                else 
                                                {
                                                   Cell.setCellValue(Output);
                                                 }
 
                              // Constant variables Test Data path and Test Data file name
                                 String filePath = System.getProperty(("user.dir") + "//src//TestData.xlsx");
                                 FileOutputStream fileOut = new FileOutputStream(filePath);
                                 ExcelWorkBook.write(fileOut);
                                 fileOut.flush();
                                 fileOut.close();
                                 }catch(Exception e){
                                  throw (e);
                                   }
                                  }
}
                
 
 