package utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import objectRepository.AtlassianHome;
import objectRepository.PageInput;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
 
public class BrowserActions {
    WebDriver driver;
    private static WebElement element = null;
 
    public static void initDriver(WebDriver driver, String BrowserName)
    {
    	if (BrowserName.equalsIgnoreCase("Firefox"))
		{
			driver = new FirefoxDriver();
		}else if (BrowserName.equalsIgnoreCase("Chrome"))
		{
			//Configuring Chrome Driver
			System.setProperty("webdriver.chrome.driver","(user.dir)" + "\\src\\Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if (BrowserName.equalsIgnoreCase("IE"))
		{
			//Configuring IE Driver
			System.setProperty("webdriver.ie.driver", "(user.dir)" + "\\src\\Drivers\\chromedriver.exe");
			driver = new InternetExplorerDriver();
		}else
		{
			System.out.println("Incorrect browser. Please use either of Firefox, Chrome or IE. You used "+BrowserName);
		}
    	driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
 
       //Method to set text in an element
       public static void SetText(WebElement element, String strText)
       {
              try{
                     if (element.isEnabled())
                     {     
                     element.sendKeys(strText);
                     }
                     else
                     {
                           System.out.println("Element disabled in SetText method"+element.getText());
                     }
              }catch(Exception e)
              {
                     System.out.println("Exception caught in SetText method - "+e);
              }
       }
      
       //Method to click on an element
       public static void ClickElement(WebElement element)
       {
              try{
                     if (element.isEnabled())
                     {
                           element.click();
                     }
                     else
                     {
                           System.out.println("Element disabled in ClickElement method"+element.getText());
                     }
              }catch(Exception e)
              {
                     System.out.println("Exception caught in ClickElement method - "+e);
              }
       }
      
      
       //Method to select a dropdown value
       public static void SelectElement(WebElement element, String strText)
       {
              try{
                     if (element.isEnabled())
                     {
                           Select selvalue = new Select(element);
                           selvalue.selectByVisibleText(strText);
                     }
              }catch(Exception e)
              {
                     System.out.println("Exception caught in SelectElement method - "+e);
              }
       }
}