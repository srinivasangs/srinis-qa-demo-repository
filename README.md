# README #


### What is this repository for? ###

This repository contains 2 exercises that were accomplished as part of the Spartez Technical exercises for Quality Assistance program.
1. Exploratory Testing Approach for Verifying the Hierarchy Functionality.docx
   This file documents the exploratory testing approach undertaken to validate the   hierarchy tree functionality in Atlassian portal.
2. myfirstproject Folder
   This folder contains the project files for running Selenium WebDriver tests for validating Creating a page functionality and Editing user Restrictions functionality.
Pre-Requisites
    To run the test, please ensure the following pre-requisites are met.
1.	Any decent Java programming IDE, preferably Eclipse Indigo
2.	Install JRE 1.7, Selenium 2.47, TestNG, Apache POI
3.	Windows environment with Firefox, Chrome and IE browsers
4.	IE Driver Server and Chrome Driver to be downloaded and placed under the project root folder as follows ->Project Root/src/Drivers.
5.	TestData.xlsx is placed in the Project Root/src/ folder. 
6.	Update the URL, Username and Password in the TestData.xlsx file before running this test.

How to run it
1.      Open the myfirstproject as a project in Eclipse IDE. 
2.      Ensure the prerequisites are met.
3.      Under the 'testSuite' package, open the 'ExecuteTests.Java' file and run it as a TestNG test.
4.      The two scenarios will be run for Chrome, IE and Firefox drivers and the results will be published.

Project Components
1. Driver Folder - Contains the driver files for Chrome and Internet Explorer.
2. objectRepository Folder - Contains the page-wise objects defined in Page object repository.
   ->AtlassianHome.java
   ->DashboardPage.java
   ->PageInput.java
   ->SetRestrictionsWindow.java
   ->ViewPage.java
3. testSuite folder - Contains the TestNG executable test file that acts as a driver for integrating multiple components and running the tests.
   ->ExecuteTests.java
4.  utilities folder - Contains functions that are used for their utilitarian value such as manipulating the excel data file, generating random numbers etc.
   ->Excelutils.java - Used for manipulating the Excel data sheets.
   ->RandomNumbers.java - Used for generating Random numbers to suffix the page title so that the tests could be run multiple times without having to encounter the "Page title exists" error message.
   ->BrowserActions.java - This file encapsulates actions such as click, enter text, select a dropdown value etc, that can be performed on a browser page. This was created to scale up the framework to a keyword driven framework and is not currently used in this project.
5.TestData.xlsx - The data sheet that supplies data to the tests.
  Contains columns for entering URL, Browser (IE/Chrome/Firefox), Username and Password. The tests will be run based on number of rows present in this data sheet.

6. seleniumtest1.java - An obsolete file that is not being used in the project.



### Who do I talk to? ###

* Srinivasan GS (Srini) - ashokdada@gmail.com - +91 9600040065